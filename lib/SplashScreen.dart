import 'dart:async';

import 'package:Araba/UI/home_page.dart';
import 'package:Araba/bloc/auth/auth_bloc.dart';
import 'package:Araba/bloc/auth/bloc.dart';
import 'package:Araba/no_user_screen.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'bloc/auth/auth_bloc.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final AuthBloc _authBloc = GetIt.instance<AuthBloc>();
  StreamSubscription _authSub;
  bool hasUser = false;

  @override
  void initState() {
    super.initState();
    _authBloc.dispatch(AppStarted());
    _authSub = _authBloc.authStateSubject.listen(
      (AuthState state) {
        if (state is AppUserState) {
          if (!state.hasUser) {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => NoUserScreen()));
          } else {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => HomePage()));
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 70),
          child: Image.asset("assets/images/icon.png"),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _authSub.cancel();
    super.dispose();
  }
}
