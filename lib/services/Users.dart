import 'package:Araba/PODO/User.dart';
import 'package:auth_provider/AppException.dart';
import 'package:auth_provider/AuthProvider.dart';
import 'package:auth_provider/UserInterface.dart';
import 'package:auth_provider/auth_methods/GraphEmailLoginMethod.dart';
import 'package:auth_provider/auth_methods/GraphEmailSignupMethod.dart';
import 'package:flutter/material.dart';
import 'package:fly_networking/GraphQB/graph_qb.dart';
import 'package:fly_networking/fly.dart';
import 'package:get_it/get_it.dart';

class UsersService {
  Fly _fly;
  final AuthProvider _authProvider = GetIt.instance<AuthProvider>();

  UsersService() {
    _fly = GetIt.instance<Fly<dynamic>>();
  }
  Future<AuthUser> signUp({
    @required String email,
    @required String password,
    @required String phone_code,
    @required String phone,
    @required String name,
  }) async {
    return await _authProvider.signUpWithEmail(
      method: GraphEmailSignupMethod(
        apiLink: "http://46.101.236.62/graphql",
        graphSignupNode: Node(
          name: "signup",
          args: {
            "type": "_EMAIL",
            "role": "_CUSTOMER",
            "email": email,
            "password": password,
            "input": {
              "name": name,
              "phone": phone,
              "phone_code": phone_code,
            }
          },
          cols: [],
        ),
      ),
    );
  }

  Future<AuthUser> _loginWithEmailCallType(AuthUser user) async {
    if (user.role != "customer") {
      throw (AppException(true,
          beautifulMsg:
              "Please double-check and try again")); //chan ge when test
    }
    return User()
      ..id = user.id
      ..jwtToken = user.token
      ..role = user.role
      ..expire = user.expire;
  }

  Future<AuthUser> login({
    @required String email,
    @required String password,
  }) async {
    AuthUser user = await _authProvider.loginWith(
      callType: _loginWithEmailCallType,
      method: GraphEmailLoginMethod(
        apiLink: "http://46.101.236.62/graphql",
        graphLoginNode: Node(
          name: "login",
          args: {
            "type": "_EMAIL",
            "email": _getCleanString(email),
            "password": password,
          },
          cols: [
            "id",
            "email",
            "role",
            "jwtToken",
            "expire",
          ],
        ),
      ),
    );
    return user;
  }

  Future<void> resetPassword(String email) async {
    Node resetPassword = Node(name: "reset_password", args: {"email": email});

    dynamic results = await _fly.mutation([resetPassword], parsers: {});
    print(results);
  }

  Future<void> changePassword(
      String currentPassword, String newPassword) async {
    Node changePassNode = Node(
      name: "reAuth_user",
      args: {
        "type": "email",
        "credentials": {
          "oldPassword": currentPassword,
          "newPassword": newPassword,
        },
      },
      cols: ['id'],
    );

    await _fly.mutation([changePassNode], parsers: {});
  }

  String _getCleanString(String value) {
    return value.trim();
  }

  void logout() {
    GetIt.instance<AuthProvider>().logout();
  }
}
