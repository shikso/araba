import 'package:flutter/cupertino.dart';
import 'package:fly_networking/GraphQB/graph_qb.dart';
import 'package:fly_networking/fly.dart';
import 'package:get_it/get_it.dart';
import 'package:notifier/FCMProvider.dart';

class NotificationService {
  void registerToFCMRefreshToken() {
    GetIt.instance.get<FCMProvider>().setFCMBackgroundMessageHandler(
          onNotification: FCMProvider.notify,
          onNotificationIOS: FCMProvider.notify,
          onNotificatioClick: FCMProvider.notify,
          onNotificatioClickIOS: FCMProvider.notify,
          iosNotificationWidget:
              (int id, String title, String body, String payload) =>
                  CupertinoAlertDialog(title: Text(title), content: Text(body)),
        );

    GetIt.instance
        .get<FCMProvider>()
        .fcmTokenSubject
        .listen((refreshedFCMToken) => updateFCMToken(refreshedFCMToken));
  }

  void updateFCMToken(String token) async {
    Node updateFCMNode = Node(
      name: "updatedFCM",
      args: {"fcm_token": token},
    );

    await GetIt.instance<Fly>().mutation([updateFCMNode]);
  }
}
