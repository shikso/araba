import 'package:Araba/PODO/Customer.dart';
import 'package:auth_provider/AuthProvider.dart';
import 'package:fly_networking/GraphQB/graph_qb.dart';
import 'package:fly_networking/fly.dart';
import 'package:get_it/get_it.dart';

class ProfileService {
  Fly _fly;
  Customer _myCustomer;

  ProfileService() {
    _fly = GetIt.instance<Fly<dynamic>>();
  }

  Customer get myCustomer=>_myCustomer;

  Future<Customer> getProfileData() async {
    if (_myCustomer != null) return _myCustomer;
    return fetchMyCustomer();
  }

  Future<Customer> fetchMyCustomer() async {
    Node myCustomerNode = Node(
      name: "myCustomer",
      cols: [
        "id",
        "name",
        "phone",
        "phone_code",
        "email",
        
        "customer_node_id",
      ],
    );
    dynamic result = await _fly.query([myCustomerNode],
        parsers: {"myCustomer": Customer.empty()});
    _myCustomer = result["myCustomer"];
    return _myCustomer;
  }

  Future<Customer> changePhoneNumber(
      String phoneCode, String phoneNumber) async {
    AuthProvider _authprovider = GetIt.instance<AuthProvider>();

    Node changePhoneNode = Node(
      name: "updateCustomer",
      args: {
        "id": _myCustomer.id,
        "phone_code": phoneCode,
       "phone": phoneNumber,
      },
      cols: ["id"],
    );

    await _fly.mutation([changePhoneNode]);
    return await fetchMyCustomer();
  }

  Future<void> refreshCustomer()async{
    _myCustomer = await fetchMyCustomer();
  }
  void clearUser(){
    _myCustomer = null;
  }
}
