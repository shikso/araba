import 'dart:async';

import 'package:Araba/UI/terms_and_conditions.dart';
import 'package:Araba/bloc/auth/bloc.dart';
import 'package:Araba/bloc/auth/auth_state.dart';
import 'package:Araba/bloc/auth/auth_event.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get_it/get_it.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  final AuthBloc _authBloc = GetIt.I<AuthBloc>();

  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();

  TextEditingController _emailcontroller = TextEditingController();

  TextEditingController _passwordcontroller = TextEditingController();

  TextEditingController _phonecontroller = TextEditingController();

  TextEditingController _phonecodecontroller = TextEditingController();

  TextEditingController _namecontroller = TextEditingController();

  StreamSubscription _authSubject;

  StreamSubscription<AuthState> _authSubscription;

  bool _showPassword = false;
  bool checkBoxValue = false;
  bool ischeckBoxErrorVisible = false;

  bool _success = false;
  String _successSignupMessgae = '';

  bool _hasError = false;
  String _errorMessage = '';

  @override
  void initState() {
    _authSubscription = _authBloc.authStateSubject.listen(
      (AuthState state) {
        if (state is SignUpSuccess) {
          setState(() => _buildSuccessSignup);
          _success = true;
          _btnController.success();
        } else if (state is SignupError) {
          setState(() => _buildErrorSignup);
          _hasError = true;

          _btnController.reset();
        }
      },
    );
    super.initState();
  }

  @override
  void dispose() {
    _authSubject.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.only(top: 25),
          child: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.brown,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
      ),
      body: Container(
        color: Colors.white,
        padding: const EdgeInsets.all(20),
        margin: new EdgeInsets.symmetric(vertical: 30.0),
        child: Form(
          key: _formkey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "New Account",
                      style: TextStyle(
                        fontSize: 30,
                      ),
                      maxLines: 1,
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                _hasError ? _buildErrorSignup(_errorMessage) : Container(),
                _success
                    ? _buildSuccessSignup(_successSignupMessgae)
                    : Container(),
                Padding(
                  padding: const EdgeInsets.only(top: 40),
                  child: TextFormField(
                    controller: _namecontroller,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: "Full Name",
                      border: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.green[300], width: 32.0),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return "This Field Is Required";
                      } else {
                        if (value.length < 5) {
                          return "name must be more than 5 charachters";
                        } else {
                          return null;
                        }
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 35,
                ),
                TextFormField(
                  controller: _emailcontroller,
                  decoration: InputDecoration(
                    hintText: "Email Address",
                    border: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.green[300], width: 32.0),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  validator: MultiValidator(
                    [
                      RequiredValidator(errorText: "This Field is Required"),
                      EmailValidator(errorText: "Invalid Email"),
                    ],
                  ),
                ),
                SizedBox(
                  height: 35,
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: TextFormField(
                        controller: _phonecodecontroller,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          hintText: "+966",
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.green[300], width: 32.0),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "This Field Is Required";
                          } else {
                            if (value.length < 3) {
                              return "Invalid";
                            } else {
                              if (value.length > 4) {
                                return "Invalid";
                              } else {
                                return null;
                              }
                            }
                          }
                        },
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Expanded(
                      flex: 3,
                      child: TextFormField(
                        controller: _phonecontroller,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          hintText: "5xxxxxxxx",
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.green[300], width: 32.0),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "This Field Is Required";
                          } else {
                            if (value.length < 9)
                              return "Phone Number must be 11 charachters";
                            else {
                              if (value.length > 11) {
                                return "Phone Number must be 11 charachters";
                              } else {
                                return null;
                              }
                            }
                          }
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 35,
                ),
                TextFormField(
                  controller: _passwordcontroller,
                  decoration: InputDecoration(
                    hintText: "Password",
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() {
                          _showPassword = !_showPassword;
                        });
                      },
                      child: Icon(
                        _showPassword ? Icons.visibility : Icons.visibility_off,
                      ),
                    ),
                    border: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.blue[300], width: 12.0),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  obscureText: !_showPassword,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "This Field Is Required";
                    } else {
                      if (value.length < 8) {
                        return "Password must be more than 8 charachters";
                      } else {
                        return null;
                      }
                    }
                  },
                ),
                SizedBox(
                  height: 30,
                ),
                CheckboxListTile(
                  contentPadding: EdgeInsets.all(10),
                  subtitle: ischeckBoxErrorVisible
                      ? Text(
                          "You must agree the terms and conditions",
                          style: TextStyle(color: Colors.redAccent[700]),
                        )
                      : null,
                  controlAffinity: ListTileControlAffinity.leading,
                  title: RichText(
                    text: TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                          text: "  I Agree to ",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.black,
                          ),
                        ),
                        TextSpan(
                          text: "Terms & Conditions",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.brown,
                            fontWeight: FontWeight.bold,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        TermsAndConditionsScreen())),
                        ),
                      ],
                    ),
                  ),
                  activeColor: Colors.green[800],
                  checkColor: Colors.white,
                  value: checkBoxValue,
                  onChanged: (bool value) {
                    setState(
                      () {
                        checkBoxValue = value;
                      },
                    );
                  },
                ),
                SizedBox(
                  height: 60,
                ),
                RoundedLoadingButton(
                    color: Colors.green[800],
                    width: 500,
                    height: 50,
                    duration: Duration(
                      milliseconds: 800,
                    ),
                    child: Text(
                      'Create Account',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    controller: _btnController,
                    onPressed: () async {
                      if (_formkey.currentState.validate() &&
                          validateCheckBox()) {
                        _authBloc.dispatch(SignUpTapped(
                          _emailcontroller.text,
                          _passwordcontroller.text,
                          _phonecontroller.text,
                          _phonecodecontroller.text,
                          _namecontroller.text,
                        ));

                        _btnController.success();
                      } else {
                        _btnController.reset();
                        _hasError = true;
                      }
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool validateCheckBox() {
    if (checkBoxValue == true) {
      return true;
    } else {
      setState(
        () {
          ischeckBoxErrorVisible = true;
        },
      );

      return false;
    }
  }

  /* bool saveAndValidate() {
    _formkey.currentState.save(); 
    return _formkey.currentState.validate();
  }*/

  Widget _buildErrorSignup(String errorMessage) {
    return Container(
      padding: const EdgeInsets.all(15),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.redAccent[700],
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Text(
            "The Email you entered is already in use ",
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            "please double-check and try again ",
            style: TextStyle(
              color: Colors.white,
              fontSize: 13,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget _buildSuccessSignup(String successSignupMessage) {
    return Container(
      padding: const EdgeInsets.all(15),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.yellow[600],
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Text(
            "Account Created Successfully",
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            "Check your email inbox for verification link",
            style: TextStyle(
              color: Colors.white,
              fontSize: 13,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
