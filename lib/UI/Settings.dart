import 'package:Araba/UI/change_password_screen.dart';
import 'package:Araba/bloc/auth/auth_bloc.dart';
import 'package:Araba/bloc/auth/bloc.dart';
import 'package:Araba/no_user_screen.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  final AuthBloc _authBloc = GetIt.instance<AuthBloc>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            "Settings",
            style: TextStyle(
              color: Colors.black,
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.brown[150],
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: Container(
          color: (Colors.white),
          child: Column(
            children: [
              Container(
                color: Colors.grey[50],
                child: Labelbr(
                  labelname: "Personal",
                  imageIcon: "",
                ),
              ),
              Divider(height: 2),
              Container(
                height: 106,
                child: Label(
                  labelname: '''Name information
 E-Mail
 Phone-Number''',
                  imageIcon: "assets/images/user.png",
                ),
              ),
              Divider(height: 2),
              InkWell(
                onTap: () {
                  _exitApp(context);
                },
                child: Container(
                  child: Label(
                    labelname: "Logout",
                    imageIcon: "assets/images/logout.png",
                  ),
                ),
              ),
              Divider(
                height: 2,
              ),
              Container(
                color: Colors.grey[50],
                child: Labelbr(
                  labelname: "Account Settings",
                  imageIcon: "",
                ),
              ),
              Divider(height: 2),
              InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ChangePasswordScreen()));
                },
                child: Container(
                  child: Label(
                    labelname: "Change Password",
                    imageIcon: "assets/images/lock.png",
                  ),
                ),
              ),
              Divider(height: 2),
              Container(
                child: Label(
                  labelname: "Change Phone Number",
                  imageIcon: "assets/images/phone-green.png",
                ),
              ),
              Divider(height: 2),
              Container(
                child: Label(labelname: "Change Language", imageIcon: ""),
              ),
              Divider(height: 2),
              Container(
                color: Colors.grey[50],
                child: Labelbr(
                  labelname: "Archive",
                  imageIcon: "",
                ),
              ),
              Divider(height: 2),
              Container(
                child: Label(
                  labelname: "Requests History",
                  imageIcon: "assets/images/history.png",
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<bool> _exitApp(BuildContext context) {
    return showDialog(
          context: context,
          child: AlertDialog(
            title: Text(
              'Are You Sure You Want To Logout ?',
              style: TextStyle(fontSize: 15),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  print("you choose no");
                  Navigator.of(context).pop(false);
                },
                child: Text('Cancel'),
              ),
              FlatButton(
                onPressed: () {
                  //SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                  _authBloc.dispatch(LogoutTapped());
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => NoUserScreen()));
                },
                child: Text(
                  'Logout',
                  style: TextStyle(color: Colors.red),
                ),
              ),
            ],
          ),
        ) ??
        false;
  }
}

class Label extends StatelessWidget {
  final String labelname;
  final String imageIcon;
  static final accentColor = Color(0xFFB78E3C);
  const Label({
    this.labelname,
    this.imageIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 22.0, left: 18),
      child: Container(
        height: 70,
        child: Row(
          children: [
            Expanded(
              child: Text(
                labelname,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Expanded(
              child: Image.asset(
                imageIcon,
                width: 25,
                height: 25,
                alignment: Alignment.bottomRight,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Labelbr extends StatelessWidget {
  final String labelname;
  final String imageIcon;
  const Labelbr({
    this.labelname,
    this.imageIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 22.0, left: 18),
      child: Container(
        height: 50,
        child: Row(
          children: [
            Expanded(
              child: Text(
                labelname,
                style: TextStyle(
                  color: Colors.brown[150],
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
