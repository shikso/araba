import 'dart:async';

import 'package:Araba/bloc/auth/auth_bloc.dart';
import 'package:Araba/bloc/auth/auth_state.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get_it/get_it.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:Araba/bloc/auth/auth_event.dart';

class ResetPasswordScreen extends StatefulWidget {
  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  final AuthBloc _authBloc = GetIt.instance<AuthBloc>();

  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();

  final TextEditingController _emailcontroller = TextEditingController();

  bool _hasError = false;
  String _errorMessage = '';

  bool _success = false;
  String _successMessgae = '';

  StreamSubscription _authSubject;

  @override
  void initState() {
    _authSubject = _authBloc.authStateSubject.listen((receivedState) {
      if (receivedState is PasswordResetInstructionsIsSent) {
        setState(() {
          _btnController.success();
          _success = true;
        });
      }

      if (receivedState is AuthExceptionIsThrown) {
        setState(() {
          _errorMessage = receivedState.exceptionMessage;
          _hasError = true;
          _btnController.reset();
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Padding(
            padding: const EdgeInsets.only(top: 25),
            child: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.brown,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ),
        ),
        body: Container(
          color: Colors.white,
          padding: const EdgeInsets.all(20.0),
          margin: new EdgeInsets.symmetric(vertical: 25.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "Reset Password",
                      style: TextStyle(fontSize: 35),
                      maxLines: 1,
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                _success ? _buildSuccessWidget(_successMessgae) :Container(),
                _hasError ? _buildErrorWidget(_errorMessage) : Container(),
                Padding(
                  padding: const EdgeInsets.only(top: 40),
                  child: Form(
                    key: _formkey,
                    child: TextFormField(
                      controller: _emailcontroller,
                      decoration: InputDecoration(
                        hintText: "Email Address",
                        border: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.green[300], width: 32.0),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                      ),
                      validator: MultiValidator(
                        [
                          RequiredValidator(
                          errorText: "This Field is Required"),
                          EmailValidator(errorText: "Invalid Email"),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 80,
                ),
                RoundedLoadingButton(
                    color: Colors.green[800],
                    width: 500,
                    height: 50,
                    duration: Duration(milliseconds: 800),
                    child: Text(
                      'Send Instructions',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    controller: _btnController,
                    onPressed: () async {
                      if (_formkey.currentState.validate()) {
                        _authBloc.dispatch(
                            PasswordResetRequested(_emailcontroller.text));
                        _btnController.success();
                        //_success = true;
                      } else {
                        _btnController.reset();
                        _hasError = true;
                      }
                    }),
              ],
            ),
          ),
        ));
  }

  Widget _buildErrorWidget(String errorMessage) {
    return Container(
      padding: const EdgeInsets.all(15),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.redAccent[700],
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Text(
            "We couldn't find your Email in our records",
            style: TextStyle(
              color: Colors.white,
              fontSize: 13,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            "please double-check and try again ",
            style: TextStyle(
              color: Colors.white,
              fontSize: 13,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget _buildSuccessWidget(String successMessage) {
    return Container(
      padding: const EdgeInsets.all(15),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.yellow[800],
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Text(
            "We sent instructions to submitted Email",
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Tap link in Email to reset password",
            style: TextStyle(
              color: Colors.white,
              fontSize: 13,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _authSubject.cancel();
    super.dispose();
  }
}
