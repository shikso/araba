import 'dart:async';

import 'package:Araba/bloc/auth/bloc.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  final AuthBloc _authBloc = GetIt.instance<AuthBloc>();

  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();

  final TextEditingController _currentPassController = TextEditingController();
  final TextEditingController _newPassController = TextEditingController();
  StreamSubscription<AuthState> _authSubscription;

  bool _hasError = false;
  String _errorMessage = '';

  bool _success = false;
  String _successChangePasswordMessgae = '';

  bool _showCurrentPassword = false;
  bool _showNewPassword = false;

  @override
  void initState() {
    super.initState();
    _authSubscription = _authBloc.authStateSubject.listen(
      (AuthState state) {
        if (state is ChangePasswordSuccess) {
          setState(() => _buildSuccessChangePassword);
          _success = true;
          _btnController.success();
        } else if (state is ChangePasswordError) {
          setState(() => _buildErrorChangePassword);
          _hasError = true;
          _btnController.reset();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Padding(
            padding: const EdgeInsets.only(top: 30),
            child: new Text(
              "Change Password",
              style: TextStyle(color: Colors.black),
            ),
          ),
          leading: Padding(
            padding: const EdgeInsets.only(top: 25),
            child: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.brown,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ),
        ),
        body: Container(
          color: Colors.white,
          padding: const EdgeInsets.all(20.0),
          margin: new EdgeInsets.symmetric(vertical: 25.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                _success
                    ? _buildSuccessChangePassword(_successChangePasswordMessgae)
                    : Container(),
                _hasError
                    ? _buildErrorChangePassword(_errorMessage)
                    : Container(),
                SizedBox(
                  height: 40,
                ),
                Form(
                  key: _formkey,
                  child: TextFormField(
                    controller: _currentPassController,
                    decoration: InputDecoration(
                      hintText: "Current Password",
                      suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            _showCurrentPassword = !_showCurrentPassword;
                          });
                        },
                        child: Icon(
                          _showCurrentPassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                      ),
                      border: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.blue[300], width: 12.0),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                    obscureText: !_showCurrentPassword,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "This Field Is Required";
                      } else {
                        if (value.length < 8) {
                          return "Password must be more than 8 charachters";
                        } else {
                          return null;
                        }
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                TextFormField(
                  controller: _newPassController,
                  decoration: InputDecoration(
                    hintText: "New Password",
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() {
                          _showNewPassword = !_showNewPassword;
                        });
                      },
                      child: Icon(
                        _showNewPassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                    ),
                    border: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.blue[300], width: 12.0),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  obscureText: !_showNewPassword,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "This Field Is Required";
                    } else {
                      if (value.length < 8) {
                        return "Password must be more than 8 charachters";
                      } else {
                        return null;
                      }
                    }
                  },
                ),
                SizedBox(
                  height: 60,
                ),
                RoundedLoadingButton(
                    color: Colors.green[800],
                    width: 500,
                    height: 50,
                    duration: Duration(milliseconds: 800),
                    child: Text(
                      'Save Changes',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    controller: _btnController,
                    onPressed: () async {
                      if (_formkey.currentState.validate()) {
                        _authBloc.dispatch(
                          ChangePasswordTapped(
                              currentPass: _currentPassController.text,
                              newPass: _newPassController.text),
                        );
                        _btnController.success();
                      } else {
                        _btnController.reset();
                        _hasError = true;
                      }
                    }),
              ],
            ),
          ),
        ));
  }

  Widget _buildErrorChangePassword(String errorMessage) {
    return Container(
      padding: const EdgeInsets.all(15),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.redAccent[700],
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Text(
            "Password is incorrect",
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            "please double-check and try again ",
            style: TextStyle(
              color: Colors.white,
              fontSize: 15,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget _buildSuccessChangePassword(String successMessage) {
    return Container(
      padding: const EdgeInsets.all(15),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.yellow[800],
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Text(
            "Success",
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Password Changed Successfully",
            style: TextStyle(
              color: Colors.white,
              fontSize: 15,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _authSubscription.cancel();
    super.dispose();
  }
}
