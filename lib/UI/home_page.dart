import 'package:Araba/UI/Drawer/navigation_drawer.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: Scaffold(
        key: _drawerKey,
        drawer: NavigationDrawer(),
        appBar: AppBar(
          backgroundColor: Colors.amber,
          title: Padding(
            padding: const EdgeInsets.only(top: 18, left: 100),
            child: Image.asset(
              "assets/images/smallLogo.png",
              height: 40,
            ),
          ),
          leading: Padding(
            padding: const EdgeInsets.only(top: 25),
            child: InkWell(
              onTap: () {
                _drawerKey.currentState.openDrawer();
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 7),
                child: Image.asset(
                  "assets/images/menu.png",
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class Drawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}
