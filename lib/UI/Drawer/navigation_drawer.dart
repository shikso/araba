import 'package:Araba/UI/Settings.dart';
import 'package:Araba/bloc/auth/auth_bloc.dart';
import 'package:Araba/bloc/auth/bloc.dart';
import 'package:Araba/no_user_screen.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class NavigationDrawer extends StatefulWidget {
  const NavigationDrawer();

  @override
  _NavigationDrawerState createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {
    final AuthBloc _authBloc = GetIt.instance<AuthBloc>();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.white,
            blurRadius: 4,
          ),
        ],
      ),
      width: 350,
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                top: 50,
                bottom: 20,
                left: 20,
                right: 8,
              ),
              child: Image.asset(
                "assets/images/icon.png",
                height: 80,
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Divider(),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => Settings()));
                },
                child: Container(
                  child: Text(
                    "Settings",
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(17.0),
              child: Container(
                child: Text(
                  "Contact Us",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(17.0),
              child: Container(
                child: Text(
                  "About Us",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(17.0),
              child: Container(
                child: Text(
                  "Rate Araba",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Container(
                child: InkWell(
                  onTap: () {
                    _exitApp(context);
                  },
                  child: Text(
                    "Logout",
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> _exitApp(BuildContext context) {
    return showDialog(
          context: context,
          child: AlertDialog(
            title: Text(
              'Are You Sure You Want To Logout ?',
              style: TextStyle(fontSize: 15),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  print("you choose no");
                  Navigator.of(context).pop(false);
                },
                child: Text('Cancel'),
              ),
              FlatButton(
                onPressed: () {
                  //SystemChannels.platform.invokeMethod('SystemNavigator.pop');
           _authBloc.dispatch(LogoutTapped());
          Navigator
              .push(
                context,MaterialPageRoute(builder:
                 (context)=> NoUserScreen()));

                },
                child: Text('Logout',style: TextStyle(color: Colors.red),),
              ),
            ],
          ),
        ) ??
        false;
  }
}


