import 'dart:async';

import 'package:Araba/UI/home_page.dart';
import 'package:Araba/UI/reset_password_screen.dart';
import 'package:Araba/bloc/auth/auth_bloc.dart';
import 'package:Araba/bloc/auth/auth_state.dart';
import 'package:Araba/bloc/auth/auth_event.dart';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:form_field_validator/form_field_validator.dart';

class LoginScreen extends StatefulWidget { 
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();

  final AuthBloc _authBloc = GetIt.instance<AuthBloc>();

  TextEditingController _emailcontroller = TextEditingController();

  TextEditingController _passwordcontroller = TextEditingController();

  StreamSubscription _authSubject;

  bool _hasError = false;
  
  String _errorMessage = '';   

  bool _showPassword = false;

  @override
  void initState() {
    _authSubject = _authBloc.authStateSubject.listen((receivedState) {
      if (receivedState is UserIsLoggedIn) {
        setState(() {
          _hasError = false;
          _btnController.success();
        });

        Future.delayed(Duration(milliseconds: 1000)).then(
          (_) => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      HomePage())), 
        );
      }

      if (receivedState is AuthExceptionIsThrown) {
        setState(() { 
          _errorMessage = receivedState.exceptionMessage;
          _hasError = true;
          _btnController.reset(); 
        });
      }
    });
    super.initState();
  }

  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.only(top: 25),
          child: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.brown,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
      ),
      body: Container(
        color: Colors.white,
        padding: const EdgeInsets.all(20.0),
        margin: new EdgeInsets.symmetric(vertical: 25.0),
        child: Form(
          key: _formkey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "Login",
                      style: TextStyle(fontSize: 35),
                      maxLines: 1,
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                _hasError ? _buildErrorLogin(_errorMessage) : Container(),
                
                Padding(
                  padding: const EdgeInsets.only(top: 40),
                  child: TextFormField(
                    controller: _emailcontroller,
                    decoration: InputDecoration(
                      hintText: "Email Address",
                      border: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.green[300], width: 32.0),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                    validator: MultiValidator(
                      [
                        RequiredValidator(errorText: "This Field is Required"),
                        EmailValidator(errorText: "Invalid Email"),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller: _passwordcontroller,
                  decoration: InputDecoration(
                    hintText: "Password",
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() {
                          _showPassword = !_showPassword;
                        });
                      },
                      child: Icon(
                        _showPassword ? Icons.visibility : Icons.visibility_off,
                      ),
                    ),
                    border: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.blue[300], width: 12.0),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  obscureText: !_showPassword,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "This Field Is Required";
                    } else {
                      if (value.length < 8) {
                        return "Password must be more than 8 charachters";
                      } else {
                        return null;
                      }
                    }
                  },
                ),
                SizedBox(
                  height: 25,
                ),
                Align(
                  alignment: AlignmentDirectional.centerEnd,
                  child: FlatButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ResetPasswordScreen()),
                      );
                    },
                    child: Text(
                      "Forgot your password? ",
                      style: TextStyle(fontSize: 15, color: Colors.brown),
                      maxLines: 1,
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                RoundedLoadingButton(
                  color: Colors.green[800],
                  width: 500,
                  height: 50,
                  duration: Duration(milliseconds: 800),
                  child: Text(
                    'Login',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  controller: _btnController,
                  onPressed: () async {
                    if (_formkey.currentState.validate()) {
                      _authBloc.dispatch(
                        LoginRequested(
                            _emailcontroller.text, _passwordcontroller.text),
                      );
                    } else {
                      _btnController.reset();
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildErrorLogin(String errorMessage) {
    return Container(
      padding: const EdgeInsets.all(15),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.redAccent[700],
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Text(
            "Incorrect E-mail or password ",
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            "please double-check and try again ",
            style: TextStyle(
              color: Colors.white,
              fontSize: 13,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
  @override
  void dispose() {
    _authSubject.cancel();
    super.dispose();
  }
}
