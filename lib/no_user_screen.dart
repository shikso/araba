import 'package:Araba/UI/Signup_Screen.dart';
import 'package:flutter/material.dart';
import 'package:Araba/UI/login_screen.dart';

class NoUserScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            arabaLogo(),
            buildLoginButton(
              "Login",
              () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen())),
            ),
            buildSignupButton(
              "I don't have an account",
              () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SignupScreen())),
            )
          ],
        ),
      ),
    );
  }
}

Widget buildLoginButton(String title, Function navigation) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
    child: FlatButton(
        color: Colors.grey[100],
        onPressed: navigation,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        child: Container(
          padding:
              const EdgeInsets.only(top: 20, bottom: 20, left: 5, right: 5),
          width: double.infinity,
          child: Row(
            children: [
              Expanded(
                child: Container(
                  child: Text(
                    title,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Expanded(
                  child: Image.asset(
                "assets/images/green-arrow.png",
                width: 25,
                height: 25,
                alignment: Alignment.topRight,
              ))
            ],
          ),
        )),
  );
}

Widget buildSignupButton(String title, Function navigation) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
    child: FlatButton(
      color: Colors.grey[100],
      onPressed: navigation,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      child: Container(
        padding: const EdgeInsets.only(top: 20, bottom: 20, left: 5, right: 5),
        width: double.infinity,
        child: Row(
          children: [
            Expanded(
              child: Container(
                child: Text(
                  title,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Expanded(
                child: Image.asset(
              "assets/images/green-arrow.png",
              width: 25,
              height: 25,
              alignment: Alignment.topRight,
            ))
          ],
        ),
      ),
    ),
  );
}

Widget arabaLogo() {
  return Padding(
    padding: const EdgeInsetsDirectional.only(
      start: 70,
      end: 70,
      bottom: 65,
    ),
    child: Image.asset("assets/images/smallLogo.png"),
  );
}
