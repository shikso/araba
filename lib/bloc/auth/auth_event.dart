abstract class AuthEvent {}

class LoginRequested extends AuthEvent {
  final String email;
  final String password;

  LoginRequested(this.email, this.password);
}

class PasswordResetRequested extends AuthEvent {
  final String email;

  PasswordResetRequested(this.email);
}

class AppStarted extends AuthEvent {}

class SignUpTapped extends AuthEvent {
 // Map<String, dynamic> values;

 final String email;
 final String password;
 final String name;
 final String phone_code;
 final String phone;
 //SignUpTapped(this.values);
  SignUpTapped(this.email,this.name,this.phone_code,this.phone,this.password);
}

class ChangePasswordTapped extends AuthEvent {
  final String currentPass;
  final String newPass;
  
  ChangePasswordTapped({this.currentPass, this.newPass});
}

class LogoutTapped extends AuthEvent {}
