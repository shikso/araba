import 'package:Araba/PODO/User.dart';
import 'package:Araba/services/NotificationService.dart';
import 'package:Araba/services/ProfileService.dart';
import 'package:Araba/services/Users.dart';
import 'package:auth_provider/AuthProvider.dart';
import 'package:Araba/bloc/auth/auth_event.dart';
import 'package:fly_networking/fly.dart';
import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';
//import 'package:Araba/UI/signup_screen.dart';

import 'bloc.dart';

class AuthBloc extends BLoC<AuthEvent> {
  PublishSubject<AuthState> authStateSubject = PublishSubject();

  final AuthProvider _authProvider = GetIt.instance<AuthProvider>();
  final UsersService _usersService = GetIt.instance<UsersService>();
  final ProfileService _profileService = GetIt.instance<ProfileService>();

  final NotificationService _notificationService =
      GetIt.instance<NotificationService>();
  Fly _fly;

  AuthBloc() {
    _fly = GetIt.instance<Fly<dynamic>>();
  }

  @override
  void dispatch(AuthEvent event) async {
    if (event is AppStarted) {
      _checkForUser();
    } else if (event is SignUpTapped) {
      _signUpWithEmail(event).catchError(
          (error) => authStateSubject.add(SignupError(error.beautifulMsg)));
    } else if (event is LoginRequested) {
      await _loginWithEmail(event.email, event.password);
    } else if (event is PasswordResetRequested) {
      await _resetPassword(event.email);
    } else if (event is ChangePasswordTapped) {
      await _changePassword(event).catchError((error) =>
          authStateSubject.add(ChangePasswordError(error.beautifulMsg)));
    } else if (event is LogoutTapped) {
      _logout();
    }
  }

  void _registerFCMProvider() async {
    _notificationService.registerToFCMRefreshToken();
  }

  void _checkForUser() async {
    bool hasUser = await _authProvider.hasUser();
    if (!hasUser) {
      authStateSubject.add(AppUserState(false));
      return;
    }
    User user = _authProvider.user;
    _fly.addHeaders({"Authorization": '${"Bearer"} ${user.jwtToken}'});
    await _profileService.getProfileData();
    if (hasUser) _registerFCMProvider();
    authStateSubject.add(AppUserState(true)); //resposne
  }

  Future<void> _signUpWithEmail(SignUpTapped event) async {
    // signup doesn't return any data about the user
    await _usersService.signUp(
      name: event.name,
      email: event.email,
      phone_code: event.phone_code,
      phone: event.phone,
      password: event.password,
    );

    authStateSubject.add(SignUpSuccess());
  }

  Future<void> _loginWithEmail(String email, String password) async {
    try {
      User user = await _usersService.login(email: email, password: password);
      _fly.addHeaders({"Authorization": '${"Bearer"} ${user.jwtToken}'});
      _registerFCMProvider();
      authStateSubject.add(UserIsLoggedIn());
    } catch (error) {
      print(error);
      authStateSubject.add(AuthExceptionIsThrown(error.beautifulMsg));
      return;
    }
  }

  Future<void> _resetPassword(String email) async {
    await _usersService.resetPassword(email);
    authStateSubject.add(PasswordResetInstructionsIsSent());
  }

  Future<void> _changePassword(ChangePasswordTapped event) async {
    await _usersService.changePassword(event.currentPass, event.newPass);
    authStateSubject.add(ChangePasswordSuccess());
  }

  dispose() {
    authStateSubject.close();
  }

  void _logout() {
    _usersService.logout();
    authStateSubject.add(UserLogedOut());
  }
}

abstract class BLoC<T> {
  void dispatch(T event);
}
