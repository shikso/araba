abstract class AuthState {}

class SignUpSuccess extends AuthState {}

class SignupError extends AuthState {
  final String errorMessage;
  SignupError(this.errorMessage);
}

class ChangePasswordSuccess extends AuthState {}

class ChangePasswordError extends AuthState {
  final errorMessage;
  ChangePasswordError(this.errorMessage);
}

class UserIsLoggedIn extends AuthState {}

class PasswordResetInstructionsIsSent extends AuthState {}

class AuthExceptionIsThrown extends AuthState {
  final String exceptionMessage;

  AuthExceptionIsThrown(this.exceptionMessage);
}

class AppUserState extends AuthState {
  final bool hasUser;
  AppUserState(this.hasUser);
}

class UserLogedOut extends AuthState {}
