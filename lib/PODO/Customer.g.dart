// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Customer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Customer _$CustomerFromJson(Map<String, dynamic> json) {
  return Customer(
    id: json['id'] as String,
    name: json['name'] as String,
    phone: json['phone'] as String,
    phone_code: json['phone_code'] as String,
    email: json['email'] as String,
    customer_node_id: json['customer_node_id'] as String,
  )
    ..language = json['locale'] as String
    ..querys = (json['querys'] as List)?.map((e) => e as String)?.toList()
    ..expire = json['expire'] as String
    ..jwtToken = json['jwtToken'] as String
    ..role = json['role'] as String
    ..type = json['type'] as String;
}

Map<String, dynamic> _$CustomerToJson(Customer instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'phone': instance.phone,
      'phone_code': instance.phone_code,
      'email': instance.email,
      'customer_node_id': instance.customer_node_id,
      'locale': instance.language,
      'querys': instance.querys,
      'expire': instance.expire,
      'jwtToken': instance.jwtToken,
      'role': instance.role,
      'type': instance.type,
    };
