
import 'package:fly_networking/fly.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Customer.g.dart';

@JsonSerializable()
class Customer implements Parser<Customer>{
  String id;
  String name;
  String phone;
  String phone_code;
  String email;
  String customer_node_id;
  

  @JsonKey(name: "locale")
  String language;

  Customer.empty();

  Customer({this.id, this.name, this.phone, this.phone_code, this.email, this.customer_node_id,});

  factory Customer.fromJson(Map<String, dynamic> json) => _$CustomerFromJson(json);

  Map<String, dynamic> toJson() => _$CustomerToJson(this);

  @override
  Customer parse(data) {
    return Customer.fromJson(data);
  }

  @override
  dynamicParse(data) {
    // TODO: implement dynamicParse
    return null;
  }

  @override
  List<String> querys = ["myCustomer"];

  @override
  String expire;

  @override
  String jwtToken;

  @override
  String role;

  @override
  String type;
}
