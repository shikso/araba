import 'dart:async';

import 'package:Araba/PODO/User.dart';
import 'package:Araba/SplashScreen.dart';
import 'package:Araba/UI/home_page.dart';
import 'package:Araba/bloc/auth/auth_bloc.dart';
import 'package:Araba/services/NotificationService.dart';
import 'package:Araba/services/ProfileService.dart';
import 'package:Araba/services/Users.dart';
import 'package:auth_provider/AuthProvider.dart';
import 'package:flutter/material.dart';
import 'package:fly_networking/fly.dart';
import 'package:get_it/get_it.dart';
import 'package:notifier/FCMProvider.dart';

import 'bloc/auth/auth_bloc.dart';
import 'bloc/auth/bloc.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AuthBloc _authBloc;
  StreamSubscription _authSub;

  @override
  void initState() {
    _initData();
    _initializeAfterLogout();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    // statusBarColor: Colors.white));
    return Material(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Araba',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Colors.blue,
          canvasColor: Colors.white,
          appBarTheme: AppBarTheme(elevation: 0, color: Colors.white),
        ),
        home: HomePage(),
      ),
    );
  }

  void _initData() {
    GetIt.instance.reset();
    String gqLink = "http://46.101.236.62/graphql";

    GetIt.instance.registerSingleton<Fly<dynamic>>(Fly<dynamic>(gqLink));
    GetIt.instance.registerLazySingleton<AuthBloc>(() => AuthBloc());

    GetIt.instance
        .registerLazySingleton<AuthProvider>(() => AuthProvider(User()));
    GetIt.instance.registerSingleton<UsersService>(UsersService());

    GetIt.instance
        .registerLazySingleton<ProfileService>(() => ProfileService());

    GetIt.instance
        .registerSingleton<NotificationService>(NotificationService());

    GetIt.instance.registerSingleton<FCMProvider>(FCMProvider());
  }

  void resetGetIt() {
    GetIt.instance.resetLazySingleton(instance: GetIt.instance<AuthBloc>());

    GetIt.instance
        .resetLazySingleton(instance: GetIt.instance<ProfileService>());
  }

  void _initializeAfterLogout() {
    _authBloc = GetIt.instance<AuthBloc>();

    _authSub = _authBloc.authStateSubject.listen((AuthState state) {
      if (state is UserLogedOut) {
        resetGetIt();
      }
    });
  }

  @override
  void dispose() {
    _authSub.cancel();
    super.dispose();
  }
}
